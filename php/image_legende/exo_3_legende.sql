-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  sam. 15 août 2020 à 14:46
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exo_3_legende`
--

-- --------------------------------------------------------

--
-- Structure de la table `legende`
--

DROP TABLE IF EXISTS `legende`;
CREATE TABLE IF NOT EXISTS `legende` (
  `image_name` varchar(30) NOT NULL,
  `legende_text` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `legende`
--

INSERT INTO `legende` (`image_name`, `legende_text`) VALUES
('triumphal_arch', 'L\'arc de triomphe se trouve à Paris!'),
('helsinski', 'Helsinki se trouve en Finlande!'),
('berlin_gate', 'La porte de Berlin se trouve en Allemagne!'),
('taj_mahal', 'Le Taj Mahal se trouve en Inde!');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
