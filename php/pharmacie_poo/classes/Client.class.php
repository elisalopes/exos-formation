<?php 

class Client {

    private $_name;
    private $_credit;

    public function __construct($name, $credit) {
        $this->setName($name);
        $this->setCredit($credit);
    }

    public function setName ($name) {
        $this->_name = $name;
    }
    
    public function getName () {
        return $this->_name;
    }

    public function setCredit ($credit) {
        $this->_credit = $credit;
    }
    
    public function getCredit () {
        return $this->_credit;
    }

    public function purchase($price) { 
        $this->_credit += $price;
    }
}