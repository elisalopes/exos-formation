<?php

class Drug {

    private $_name;
    private $_price;
    private $_stock;

    public function __construct($name, $price, $stock) {
        $this->setName($name);
        $this->setPrice($price);
        $this->setStock($stock);
    }

    public function setName ($name) {
        $this->_name = $name;
    }
    
    public function getName () {
        return $this->_name;
    }

    public function setPrice ($price) {
        $this->_price = $price;
    }
    
    public function getPrice () {
        return $this->_price;
    }

    public function setStock ($stock) {
        $this->_stock = $stock;
    }
    
    public function getStock () {
        return $this->_stock;
    }

    public function purchase($quantity) {
        if ($quantity > $this->_stock) {
            return "Impossible d'acheter plus que le stock!";
        }
        $this->_stock -= $quantity;
        return NULL; // tout s'est bien passé
    }

    public function supply($quantity) {
        $this->_stock += $quantity;
    }
}