<?php

class Pharmacy {

    private $_clients = array();
    private $_drugs = array();
    private $_name;

    //TEST
    // public function __construct($clients, $drugs, $name) {
    //     $this->setClients($clients);
    //     $this->setDrugs($drugs);
    //     $this->setName($name);
    // }

    // SETTERS AND GETTERS

    public function setClients ($clients) {
        $this->_clients = $clients;
    }
    
    public function getClients () {
        return $this->_clients;
    }

    public function setDrugs ($drugs) {
        $this->_drugs = $drugs;
    }
    
    public function getDrugs () {
        return $this->_drugs;
    }

    public function setName ($name) {
        $this->_name = $name;
    }
    
    public function getName () {
        return $this->_name;
    }

    // ACTIONS
    public function displayData() {
        echo "Voici la liste des clients:\n";
        //afficher clients et leurs credits respectifs
        for ($i = 0; $i < count($this->_clients); $i++) {
            echo $this->_clients[$i]->getName().' - credit: '.$this->_clients[$i]->getCredit()."$\n";
        }
        echo "Voici la liste des medicaments:\n";
        // afficher medicaments et stocks
        for ($i = 0; $i < count($this->_drugs); $i++) {
            echo $this->_drugs[$i]->getName().' - stock: '.$this->_drugs[$i]->getStock().' - prix: '.$this->_drugs[$i]->getPrice()."$\n";
        }
    }

    public function supply($drug_name, $quantity) {
        // verifier que le nom medicament existe bien 
        $drug = $this->readDrugData($drug_name);
        if (is_null($drug)) {
            return "Médicament non trouvé!";
        }
        $drug->supply($quantity);
    }

    public function readDrugData($drug_name) {
        // boucler pour verifier
        for ($i = 0; $i < count($this->_drugs); $i++) {
            if ($drug_name == $this->_drugs[$i]->getName()) {
                return $this->_drugs[$i];
            }
        }
        return NULL;
        // utilise par methode achat + approvisionnement
    }

    public function readClientData($client_name) {
        //boucler pour verifier dans liste
        for ($i = 0; $i < count($this->_clients); $i++) {
            if ($client_name == $this->_clients[$i]->getName()) {
                 // retourner le client
                return $this->_clients[$i];
            }
        }
        return NULL;
       
        // utilise par methode achat 
    }

    public function purchase($drug_name, $client_name, $quantity) {
        // achat d'un client, medicament donne sur quantite donnee
        $drug = $this->readDrugData($drug_name);
        $client = $this->readClientData($client_name);
        if (is_null($client)) {
            return "Client non trouvé!";
        }
        //deduire medic du stock et augmenter credit client
        $price = $drug->getPrice() * $quantity;
        $is_okay = $drug->purchase($quantity);
        if (is_null($is_okay)) {
            $client->purchase($price);
        } else {
            return $is_okay;
        }
    }

    public function closeProgram() {
        // affiche message programme termine
        echo "Programme terminé!";
    }

    public function populate($name, $client, $drug) { // recréer ma classe a partir de ce que j'ai en bdd
        $this->setName($name);
        for ($i=0; $i < count($client); $i++) {
            $new_client = new Client($client[$i]['client_name'], $client[$i]['client_credit']);
            array_push($this->_clients, $new_client);
            
        }
        for ($j=0; $j < count($drug); $j++) {
            $new_drug = new Drug($drug[$j]['drug_name'], $drug[$j]['drug_price'], $drug[$j]['drug_stock']);
            array_push($this->_drugs, $new_drug);
        }
    }
}