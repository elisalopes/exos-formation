<?php

class PharmacyManager {

    private $_db;

    public function __construct($db) {
        $this->setDb($db);
    }

    public function setDb ($db) {
        $this->_db = $db;
    }

    public function getPharmacy($pharmacy_name) {
        try{
            //requetes
            $query = $this->_db->prepare("
                SELECT pharmacy_id FROM pharmacy WHERE pharmacy_name=?
            ");
            $query->execute(array($pharmacy_name));
            $result = $query->fetch(PDO::FETCH_ASSOC);

            $query = $this->_db->query("SELECT * FROM client WHERE pharmacy_id=".$result['pharmacy_id']); // query(securise pas l'info) comme prepare execute
            $clients = $query->fetchAll(PDO::FETCH_ASSOC);

            $query = $this->_db->query("SELECT * FROM drug WHERE pharmacy_id=".$result['pharmacy_id']);
            $drugs = $query->fetchAll(PDO::FETCH_ASSOC);

            $pharmacy = new Pharmacy();
            $pharmacy->populate($pharmacy_name, $clients, $drugs);
            return $pharmacy;

        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function updateClientCredit($client) {
        try{
            //requetes
            $query = $this->_db->prepare("
                UPDATE client SET client_credit=? WHERE client_name=?
            ");
            $query->execute(array($client->getCredit(), $client->getName()));

        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }

    public function updateDrugStock($drug) {
        try{
            //requetes
            $query = $this->_db->prepare("
                UPDATE drug SET drug_stock=? WHERE drug_name=?
            ");
            $query->execute(array($drug->getStock(), $drug->getName()));

        }
        catch(PDOException $e){
            echo "Erreur : " . $e->getMessage();
        }
    }


}