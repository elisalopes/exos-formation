<?php

function chargerClasse($classe)
{
  require 'classes/'.$classe . '.class.php'; 
}

spl_autoload_register('chargerClasse');

include 'connection_db.php';

$db_pharmacy = new PharmacyManager($dbh); // gestionnaire bdd gere les relations entre la classe et la bdd
$current_pharmacy = $db_pharmacy->getPharmacy('Antigone'); // recupere la pharmacie et les données

while(true) { // boucle infinie
    echo "Bonjour, pharmacie Antigone! Tapez 1 pour acheter, tapez 2 pour approvisionner, tapez 3 pour afficher les informations de la pharmacie ou tapez 4 pour quitter! \n";
    $line = readline(); // ce que l'user a tape
    if($line == '1'){ // achat medciament par user
        do { // demande au moins 1 fois
            $user = readline("Nom de l'utilisateur: ");
        } while (is_null($current_pharmacy->readClientData($user))); // si bon passe a la suite sinon il renvoie le do

        do {
            $drug_name = readline("Nom du medicament: ");
        } while (is_null($current_pharmacy->readDrugData($drug_name)));

        $stock = readline("Combien: ");
        $res_purchase = $current_pharmacy->purchase($drug_name, $user, $stock); // action principal cf class pharmacy
        if (is_null($res_purchase)) { // si tout s'est bien passe j'update 
            $db_pharmacy->updateClientCredit($current_pharmacy->readClientData($user)); //update credit client bdd
            $drug_info = $current_pharmacy->readDrugData($drug_name); // recuperer le medicament lié au nom
            $db_pharmacy->updateDrugStock($drug_info); // update stock des medicaments
            $total_price = $drug_info->getPrice() * $stock; // prix achat 
            echo "Vous avez choisi: ".$drug_name.", vous en avez pris: ".$stock.", pour un total de: ".$total_price."$\n";
        } else { // sinon affichage d'erreur
            echo $res_purchase."\n";
        } 
    } elseif ($line == '2') { // approvisionnement medicaments
        do {
            $drug_name = readline("Nom du medicament: ");
        } while (is_null($current_pharmacy->readDrugData($drug_name)));

        $stock = readline("Combien: ");

        $res_supply = $current_pharmacy->supply($drug_name, $stock);
        if (is_null($res_supply)) { // si tout s'est bien passe j'update
            $db_pharmacy->updateDrugStock($current_pharmacy->readDrugData($drug_name));
        } else { // sinon affichage d'erreur
            echo $res_supply."\n";
        }
    } elseif ($line == '3') {
        $current_pharmacy->displayData(); // affiche donnees pharmacie
    } elseif ($line == '4') {
        break; // stop boucle infinie
    }

}
$current_pharmacy->closeProgram(); // quitte programme


// TESTS
// $client1 = new Client('lili', 25);
// $client2 = new Client('lulu', 20);
// $drug1 = new Drug('doliprane', 2, 25);
// $drug2 = new Drug('spasfon',3, 25); 
// $current_pharmacy = new Pharmacy(array($client1, $client2), array($drug1, $drug2), 'antigone'); 

