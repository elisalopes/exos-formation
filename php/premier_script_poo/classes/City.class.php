<?php

class City {

    private $_cityName;
    private $_cityDepartment;

    public function __construct ($name, $department) {
        $this->setCityName($name);
        $this->setCityDepartment($department);
    }

    public function setCityName ($cityName) {
        $this->_cityName = $cityName;
    }
    
    public function getCityName () {
        return $this->_cityName;
    }
    
    public function setCityDepartment ($cityDepartment) {
        $this->_cityDepartment = $cityDepartment;
    }
    
    public function getCityDepartment () {
        return $this->_cityDepartment;
    }
    
    public function displayCity () {
        return "ville: ".$this->_cityName." (".$this->_cityDepartment.")";
    }
}

?>