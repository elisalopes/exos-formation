<?php

class Team {
    private $_members;

    public function generateMembers($number) {
        $last_names = array("Dupond", "Dupont", "Martin", "Durand", "Lopes");
        $first_names = array("Théo", "Julien", "Noémie", "Alicia", "Elisa");
        $cities = array("Nancy", "Perpignan", "Grenoble", "Dijon", "Montpellier");
        $this->_members = array();
        for ($i = 0; $i < $number; $i++) {
            $rand_last_names = $last_names[rand(0, count($last_names)-1)];
            $rand_first_names = $first_names[rand(0, count($first_names)-1)];
            $rand_cities = $cities[rand(0, count($cities)-1)];
            $rand_departments = rand(1, 95);
            array_push($this->_members, new Person($rand_last_names, $rand_first_names, $rand_cities, $rand_departments));
        }
        
    }

    public function hire($lastName, $firstName, $city, $department) {
       $new_member = new Person($lastName, $firstName, $city, $department);
       array_push($this->_members, $new_member);
       return $new_member;
    }

    public function fire($member) {
        for ($i = 0; $i < count($this->_members); $i++) {
            if ($this->_members[$i] == $member) {
                array_splice($this->_members, $i, 1);
                break;
            }
        }
    }

    public function displayTeamNames() {
        $team_names = array();
        for ($i = 0; $i < count($this->_members); $i++) {
            $last_name = $this->_members[$i]->getLastName();
            $first_name = $this->_members[$i]->getFirstName();
            array_push($team_names, $last_name.' '.$first_name);
        }
        return $team_names;
    }

    public function displayTeamDepartments() {
        $team_departments = array();
        for ($i = 0; $i < count($this->_members); $i++) {
            $departments = $this->_members[$i]->getAddress()->getCityDepartment();
            array_push($team_departments, $departments);
        }
        return $team_departments;
    }
}
?>