<?php

class Person {
    private $_personLastName;
    private $_personFirstName;
    private $_personAddress;

    public function __construct($lastName, $firstName, $city, $department) {
        $this->setLastName($lastName);
        $this->setFirstName($firstName);
        $this->setAddress($city, $department);
    }

    public function __destruct() {
        echo "supprimé!";
    }

    public function setLastName ($lastName) {
        $this->_personLastName = $lastName;
    }
    
    public function getLastName () {
        return $this->_personLastName;
    }

    public function setFirstName ($firstName) {
        $this->_personFirstName = $firstName;
    }
    
    public function getFirstName () {
        return $this->_personFirstName;
    }
    
    public function setAddress ($city, $department) {
        $this->_personAddress = new City($city, $department);
    }
    
    public function getAddress () {
        return $this->_personAddress;
    }
    
    public function getPerson () {
        return "La personne s'appelle: ".$this->_personLastName." ".$this->_personFirstName.". Elle habite la ".$this->_personAddress->displayCity();
    }
}

?>