<?php

class Form {

    private $_formHeader;
    private $_formBody;
    private $_formFooter;

    public function __construct () {
        $this->_formHeader = "<form><fieldset>";
        $this->_formFooter = "</fieldset></form>";
        $this->_formBody = "";
    }

    public function setText($name) {
        $this->_formBody .= "<input type='text' name='".$name."'/></br></br>";
    }

    public function setSubmit($value) {
        $this->_formBody .= "<input type='submit' value='".$value."'/>";
    }

    public function getForm() {
        return $this->_formHeader.$this->_formBody.$this->_formFooter;
    }
}