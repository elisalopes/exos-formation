<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function chargerClasse($classe)
{
  require 'classes/'.$classe . '.class.php'; // On inclut la classe correspondante au paramètre passé.
}

spl_autoload_register('chargerClasse'); // On enregistre la fonction en autoload pour qu'elle soit appelée dès qu'on instanciera une classe non déclarée.


// //PARTIE A
// $first_city = new City();
// $first_city->setCityName("Nancy");
// $first_city->setCityDepartment(54);
// echo $first_city->displayCity();

// PARTIE B
// $first_city = new City("Nancy", 54);
// echo $first_city->displayCity();

// PARTIE C
// $first_person = new Person("LOPES", "Elisa", "Nancy", 54);
// echo $first_person->getPerson();

// PARTIE D

// $team = new Team();
// $team->generateMembers(3);
// echo "<pre>";
// var_dump($team->displayTeamNames());
// echo "</pre>";
// echo "<pre>";
// var_dump($team->displayTeamDepartments());
// echo "</pre>";
// $new_member = $team->hire("Smith", "Lola", "Nice", 65);
// echo "<pre>";
// var_dump($team->displayTeamNames());
// echo "</pre>";
// $team->fire($new_member);
// echo "<pre>";
// var_dump($team->displayTeamNames());
// echo "</pre>";

// // PARTIE E
$first_form = new Form();
$first_form->setText("last_name");
$first_form->setText("first_name");
$first_form->setSubmit("Envoyer");
echo $first_form->getForm();

?> 