<?php

session_start();

include 'user_db.php'; // importer le fichier pour les futures fonctions

if (!empty($_SESSION["connected_admin"])) { // verifie si admin connecté au préalable
    header("location:admin.php");
} else if (!empty($_SESSION["connected"])) { // verifie si user est connecté au préalable
    header("location:infos_perso.php");
}

if (!empty($_POST["connection"])) { // si on a envoyer le form connexion
    $login = $_POST["login"];
    $password = $_POST["password"];
    if (!empty($login) && !empty($password)) { // verifie si login et password sont remplit
        if (user_connection($login, $password) == 1) { // verifie si c'est la bonne personne
            $user = get_user($login); // récupère infos user
            $_SESSION["connected"] = $user["user_id"]; // permet de savoir qu'un user est connecté et qui (quel id)
            if ($user['is_admin'] == 1) { // verifie si c'est l'admin
                $_SESSION["connected_admin"] = true;
                header("location:admin.php"); // redirige vers page admin
            } else {
                header("location:infos_perso.php"); // redirige vers la page info perso 
            }
        } else { 
            $connection_error = "Mot de passe incorrect!";
        }
    } else {
        $connection_error = "Veuillez remplir le nom d'utilisateur et le mot de passe!";
    }
        
}

if (!empty($_POST["inscription"])) { // si on a envoyé le form inscription
    if( // verifie que chaque champs n'est pas vide
        !empty($_POST["last_name"]) &&
        !empty($_POST["first_name"]) &&
        !empty($_POST["login"]) &&
        !empty($_POST["password"]) &&
        !empty($_POST["confirm_password"]) &&
        !empty($_POST["gender"]) &&
        !empty($_POST["age"]) &&
        !empty($_POST["weight"]) &&
        !empty($_POST["height"]) &&
        !empty($_POST["weight_goal"])
    ) { 
        if ($_POST["password"] == $_POST["confirm_password"]) { // verifie password et la confirmation password
            $inscription_error = user_creation($_POST); // créer utilisateur
            if (!$inscription_error) { // connecte l'user si il a été créer
                $user = get_user($_POST['login']);  
                $_SESSION["connected"] = $user["user_id"];
                header("location:infos_perso.php");
            }
        } else {
            $inscription_error = "Mot de passe pas identique!";
        }
        
    } else {
        $inscription_error = "Veuillez remplir tous le formulaire!";
    }
}

?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Page d'acceuil</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="main">
        <div class="container">
            <form action="" method="POST">
                <input type="hidden" name="connection" value="1">
                <h1>Connexion</h1>
                <div>
                    <label><b>Nom d'utilisateur</b></label>
                    <input type="text" placeholder="Entrer le nom d'utilisateur" name="login" required> 
                </div>
                <div>
                    <label><b>Mot de passe</b></label>
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required> 
                </div>
                <input type="submit" id='submit' value='CONNEXION'> 
            </form>
            <div>
            <?php 
            if (!empty($connection_error)) {
                echo $connection_error;
            }
            ?>
            </div>
        </div>
        <div class="container">
            <form action="" method="POST">
                <input type="hidden" name="inscription" value="1">

                <h1>Inscription</h1>
                <div>
                    <label><b>Nom</b></label>
                    <input type="text" placeholder="Nom" name="last_name" required> 
                </div>
                <div>
                    <label><b>Prénom</b></label>
                    <input type="text" placeholder="Prenom" name="first_name" required> 
                </div>
                <div>
                    <label><b>Nom d'utilisateur</b></label>
                    <input type="text" placeholder="Nom d'utilisateur" name="login" required> 
                </div>
                <div>
                    <label><b>Mot de passe</b></label>
                    <input type="password" placeholder="Mot de passe" name="password" required> 
                </div>
                <div>
                    <label><b>Confirmez mot de passe</b></label>
                    <input type="password" placeholder="Confirmez mot de passe" name="confirm_password" required> 
                </div>
                <div>
                    <label><b>Sexe</b></label>
                    <div>
                        <input type="radio" id="woman" name="gender" value="w">
                        <label for="woman">Femme</label>
                        <input type="radio" id="man" name="gender" value="m">
                        <label for="man">Homme</label>
                  
                        <input type="radio" id="other" name="gender" value="o" checked>
                        <label for="other">Autre</label>
                    </div> 
                </div>
                <div>
                    <label><b>Âge</b></label>
                    <span><input type="number" id="age" name="age" max="120" required> ans</span> 
                </div>
                <div>
                    <label><b>Numéro de téléphone</b></label>
                    <input id="phone_number" name="phone_number" type="tel" pattern="[0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}" placeholder="01 23 54 65 25"> 
                </div>
                <div>
                    <label><b>Poids</b></label>
                    <span><input type="number" id="weight" name="weight" max="300" step="any" required> kg</span> 
                </div>
                <div>
                    <label><b>Taille</b></label>
                    <span><input type="number" id="height" name="height" max="250" required> cm</span> 
                </div>
                <div>
                    <label><b>Objectif de poids</b></label>
                    <span><input type="number" id="weight_goal" name="weight_goal" max="300" step="any" required> kg</span> 
                </div>
                
                <input type="submit" id='submit' value='INSCRIPTION'>
            </form>
            <div>
            <?php 
            if (!empty($inscription_error)) {
                echo $inscription_error;
            }
            ?>
            </div>
        </div>
    </div>
</body>
</html>