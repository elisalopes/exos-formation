<?php

include "user_db.php";

function get_sports () { // recupérer les sports
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT * FROM sport");
    $result = $query->fetch_all(MYSQLI_ASSOC);
    mysqli_close($handler);
    return $result;
}


function activity_creation ($user_id, $sport, $duration) { // créer une activité
    $handler = mysql_connection();
    $query = mysqli_query($handler, "INSERT INTO activity (`activity_practice_time`, `user_user_id`, `sport_sport_id`) VALUES ($duration, '$user_id', '$sport')");
    mysqli_close($handler);
    if($query) {
        return Null;
    }else {
        return "Une erreur innatendue est arrivée!";
    };
}



function retrieve_today_activity ($user_id) { // récupérer l'activité journalière d'un user
    $date = date("Y-m-d");
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT * FROM activity INNER JOIN sport ON activity.sport_sport_id = sport.sport_id WHERE user_user_id = '$user_id' AND DATE(activity_date) = '$date'");
    $result = $query->fetch_all(MYSQLI_ASSOC);
    mysqli_close($handler);
    return $result;
}