<?php 

function mysql_connection () { // connexion a la base de données 
    $handler = mysqli_connect("localhost:3308", "root", "", "fitness_db");

    if (!$handler) {
        echo "erreur de connexion!";
        die;
    }

    return $handler;
}

function get_user ($login) { // permet de recuperer les infos de user à partir de son login
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT * FROM user WHERE login='$login'" );
    $result = $query->fetch_array();
    mysqli_close($handler);
    return $result;
}

function get_first_name ($user_id) { // permet de recuperer les infos de user à partir de son login
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT first_name FROM user WHERE user_id='$user_id'" );
    $result = $query->fetch_array();
    mysqli_close($handler);
    return $result[0];
}

function user_connection ($login, $password) { // permet de voir si c'est la bonne personne à partir du login et password
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT password FROM user WHERE login='$login'");
    $result = $query->fetch_array();
    mysqli_close($handler);
    return password_verify($password, $result["password"]);
}

function user_exists ($login) { // permet de voir si l'user existe déjà
    $handler = mysql_connection();
    $query = mysqli_query($handler, "SELECT COUNT(user_id) FROM user WHERE login='$login'");
    $result = $query->fetch_array();
    mysqli_close($handler);
    return $result[0];
}

function user_creation ($data) { // permet de créer l'user en bdd
    $already_exists = user_exists($data["login"]);
    if ($already_exists ==  1) { // 
        return "L'utilisateur existe déjà!";
    }
    $handler = mysql_connection();
    $phone_number = "NULL";
    $crypted_password = password_hash($data["password"], PASSWORD_BCRYPT); // chiffré mot de passe 
    if (!empty($data['phone_number'])) { // si pas remplit j'envois NULL sinon j'utilise l'info du phone
        $phone_number = '"'.$data['phone_number'].'"';
    }
    $query = mysqli_query(
        $handler, 
        'INSERT INTO `user` (
            `login`, 
            `password`, 
            `is_admin`, 
            `last_name`, 
            `first_name`, 
            `gender`, 
            `age`, 
            `phone_number`, 
            `user_weight`, 
            `user_height`, 
            `weight_goal`
        ) VALUES ('.
        '"'.$data['login'].'",'.
        '"'.$crypted_password.'",'.
        'FALSE,'.
        '"'.$data['last_name'].'",'.
        '"'.$data['first_name'].'",'.
        '"'.$data['gender'].'",'.
        $data['age'].','.
        $phone_number.','.
        $data['weight'].','.
        $data['height'].','.
        $data['weight_goal'].
    ')'
        );
    mysqli_close($handler);
    if($query) {
        return Null;
    }else {
        return "Une erreur innatendue est arrivée!";
    }
}
