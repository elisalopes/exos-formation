<?php

session_start();

include "activity_db.php"; // importer fonctions du fichier activity_db

if (empty($_SESSION["connected"])) { // verifie si il est pas connecté
    header("location:index.php"); // redirige vers index
}

if (!empty($_POST["activity"])) { // verifie l'envois du form activity
    if (!empty($_POST["sport"]) && !empty($_POST["duration"])) { // verifie que les champs sont pas vides
        $activity_error = activity_creation($_SESSION["connected"],$_POST["sport"], $_POST["duration"]); // creation activité
    } else {
        $activity_error = "Veuillez remplir tous le formulaire!";
    } 
}
?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Infos Personnelles</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
    <div id="main">
        <div>
            <?php
            echo "Bienvenue ".get_first_name($_SESSION['connected'])."!";
            ?>
        </div>
        <div class="container">
            <form action="" method="POST">
                <input type="hidden" name="activity" value="1">

                <div>
                    <h1>Sports</h1>
                    <div>
                        <?php
                            $result = get_sports(); // récupérer et afficher les sports
                            for ($i = 0; $i < count($result); $i++) { //affichage sports et leurs images
                                echo "<span class='sport_radio'>";
                                echo "<input type='radio' name='sport' value='".$result[$i]['sport_id']."'>";
                                echo "<label>".$result[$i]['sport_name']."</label>";
                                echo "<img src='sport_icons/".$result[$i]['sport_image'].".jpg' width=50>";
                                echo "</span>";
                            }
                        ?>
                    </div>
                </div>
                </br>
                <div>
                    <label><b>Temps</b></label>
                    <span><input type="number" name="duration" step="1"required> heure(s)</span> </br> </br>
                </div>
                
                <input type="submit" id='submit' value='VALIDER'>
            </form>
            <div>
            <?php 
            if (!empty($activity_error)) {
                echo $activity_error;
            }
            ?>
            </div>
        </div>
        <div class="container">
            <?php
                $activity = retrieve_today_activity($_SESSION['connected']); // récupère activité journalière de l'user
                echo "<table id='daily_activity' cellpadding=0 cellspacing=0>"; // création tableau journalier
                echo "<tr>";
                echo "<th>SPORT</th>";
                echo "<th>DURÉE</th>";
                echo "<th>CALORIES</th>";
                echo "</tr>";
                for ($i = 0; $i < count($activity); $i++) { // parcoure les activités
                    echo "<tr>";
                    echo "<td>".$activity[$i]['sport_name']."</td>";
                    echo "<td>".$activity[$i]['activity_practice_time']."h</td>";
                    echo "<td>".($activity[$i]['activity_practice_time'] * $activity[$i]['sport_energy'])."Kcal</td>";
                    echo "</tr>";
                }
                echo "</table>";
            ?>
        </div>
    </div>
    <a href='log_out.php' class="log_out">DECONNEXION</a>
</body>
</html>