let user_score = 0;
let computer_score = 0;
let round_max = 3;
let current_round = 0;

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function compare (user_choice, computer_choice) {
    let result_text = "";
    
    if (user_choice == computer_choice) {
         result_text = "Egalité.";
         current_round -= 1;
    } else if (user_choice == "pierre" && computer_choice == "ciseaux") {
         result_text = "Vous avez gagné cette manche.";
        user_score += 1;
    } else if (user_choice == "feuille" && computer_choice == "pierre") {
         result_text = "Vous avez gagné cette manche.";
        user_score += 1;
    } else if (user_choice == "ciseaux" && computer_choice == "feuille") {
         result_text = "Vous avez gagné cette manche."; 
        user_score += 1;
    } else {
        result_text = "Vous avez perdu cette manche";
        computer_score += 1;
    }
    result_text += "<br><br>Le choix de l'ordinateur etait : " + computer_choice;
    console.log(result_text);
    let result = document.getElementById("result");
    result.innerHTML = result_text;
    let user_score_display = document.getElementById("user_score_display");
    user_score_display.innerText = user_score;
    let computer_score_display = document.getElementById("computer_score_display");
    computer_score_display.innerText = computer_score;

}

function let_go () {
    console.log("Let's go !");
    let end_game = document.getElementById("end_game");
        end_game.innerText = "";
    let user_choice = document.getElementById("propositions").value;
    let choices = ["pierre", "feuille", "ciseaux"];
    let computer_choice = choices[getRandomInt(0, 3)];
    compare(user_choice, computer_choice);
    current_round += 1;
    let round = document.getElementById("round");
        round.innerText = current_round;
    // fin de partie 
    if (current_round == round_max) {
        let result_end_game = "";
        user_score = 0;
        computer_score = 0;
        current_round = 0;
        // qui a gagné
        if (user_score > computer_score) {
            result_end_game = "Fin de partie !, Tu as gagné";
        } else {
            result_end_game = "Fin de partie ! Tu as perdu";
        }
        end_game.innerText = result_end_game;
    }
    
}

