// Jour 1

function parse_csv (input, sep_rows="\n", sep_cols=",", ignore_first=false) {
    let res = [];
    let rows = input.split(sep_rows);
    let i = 0;

    if (ignore_first == true) {
        i = 1;
    }

    for (; i < rows.length; i++) {
        let cols = rows[i].split(sep_cols);
        res.push(cols);

    }
    return res;
}

parse_csv("p,q\nr,s", "\n", ",", true);


// Jour 2



// Jour 3


// Jour 4 page tableau prenom html


// Jour 5

function defere (func, arg) {
    setTimeout(function() {
        func(arg);
    },
    1000);
}

defere(console.log, "a");
console.log("b");

// Jour 6

let obj = {
    town: "New-Orleans",
    country: "USA"
};

function return_key (obj, key) {
    return obj[key];
}

return_key(obj, "country");


// Jour 11

function convert (elements, id_list) {
    let res = "<ul id='" + id_list + "'>";

    for (let i = 0; i < elements.length; i++) {
        res += "<li>" + elements[i] + "</li>";
    }

    res += "</ul>";
    return res;
}

convert(["item1", "item2"], "mylist");
