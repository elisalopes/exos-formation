// Jour 1

function clean_string (input) {
    let res = "";
    for (let i = 0; i < input.length; i++) {
        if (res.indexOf(input[i]) == -1) {
            res += input[i];
        }
    }

    return res;
}

clean_string("je suis Elisa");


// Jour 2

function change_number (input) {
    let str_input = String(input);
    if (str_input.length % 2 !== 0) {
        return input;
    }
    let res = "";
    for (let i = 0; i < str_input.length; i += 2) {
        res += str_input[i + 1];
        res += str_input[i];
        
    }

    return parseInt(res);
}

change_number("1234");


// Jour 3

function adding (input) {
    let res = 0;
    for (let i = 0; i < input.length; i++) {
      if (isNaN(input[i]) == false) {
        res += parseInt(input[i]);
      }  
    }
    return res;
}

adding("lkdieopr1dsdshe9ebhdhe10");

// façon de pierre

function sum_digits_from_string(dstr) {
    let dsum = 0;
    for (let i = 0; i < dstr.length; i++)
    {
      if (/[0-9]/.test(dstr[i])) dsum += parseInt(dstr[i])
    }
    return dsum;
  }
  console.log(sum_digits_from_string("abcd12efg9"))

// Jour 4

function calcul_volume_cube (num) {
    return num * num * num;
}

function main (input) {
    let res = 0;
    for (let i = 0; i < input; i++) {
       res += calcul_volume_cube(i + 1);
    }
    return res;
}

main(3);

// Jour 5

function transform_url (url) {
    let res = [];
    let temp = url.split("://");
    res.push(temp[0]);
    temp = temp[1].split(".");
    let param = temp[temp.length - 1].split("/");
    temp.pop()
    res = res.concat(temp);
    res = res.concat(param);

    return res;
}

transform_url("protocole://sousdomaine.domaine.extension/parametres");

// Jour 6

function sum_int (input) {
    let res = 0;
    for (let i = 0; i < input; i++) {
        res += i;
        if (res == input) {
            return i;
        }
    }
    return 0;
}

sum_int(6);

// Jour 7 

let tab_1 = [1, 2, 3];
let tab_2 = [2, 1, 4];

function compare (tab_1, tab_2) {
    let res = [];

    for (let i = 0; i < tab_1.length; i++) {

        for (let x = 0; x < tab_2.length; x++) {
            if (tab_1[i] == tab_2[x]) {
                res.push(tab_1[i]);
            }
        }
    }

    return res;

}

compare(tab_1, tab_2);


// Jour 8

let my_string = "ahe3hjf6hej";

function replace_first_number (input) {
    for (let i = 0; i < input.length; i++) {
        if (isNaN(input[i]) == false) {
            input = input.replace(input[i], "$");
            break;
        }
       
    }

    return input;
}

replace_first_number(my_string);


// Jour 9

function age (birthday) {
    let birth_year = new Date(birthday);
    let today = new Date();
    
    let res = today - birth_year;

    res = new Date(res).getFullYear() - 1970;

    return res;

}

age("1998/11/27");

// Jour 10 soustraction

// Jour 11 page pari.html

// Jour 12 

function inverse (input) {
    let res = "";

    for (let i = 0; i < input.length; i++) {
        if (input[i] == 1) {
            res += "0";
        } else {
            res += "1";
        }
    }
    return parseInt(res, 2);

}

inverse("111000");

// Jour 13 

function verify (input) {
    if (input <= 100) {
        return false;
    }

    let input_str = input.toString();

    for (let i = 0; i < input_str.length - 1; i++) {
        if (parseInt(input_str[i]) != parseInt(input_str[i + 1]) - 1) {
            return false;
        }
    }
    return true;
}

// Jour 14

function replace (input) {
    let res = [];
    

    for (let i = 0; i < 10; i++) {
        let temp = input.replace("#", i);
        if (temp % 3 == 0) {
            res.push(temp);
        }
    }

    return res;
}


// Jour 15
function random_color () {
    let tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
    let res = "#";

    for (let i = 0; i < 6; i++) {
        let color = Math.floor(Math.random()  * 16);
        res += tab[color];
    }
    return res;
}

