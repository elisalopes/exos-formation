Faire une fonction javascript pour : 

EXERCICE 1 :  VALIDE
Trouver et afficher l'URL de la page chargée 

EXERCICE 2 : VALIDE
Trouver l'extension d'un fichier (passé en chaine)

EXERCICE 3 : VALIDE
Calculer parmi 3 entiers lequel est le plus proche de 100 

EXERCICE 4 : VALIDE
Calculer la moyenne d'un élève pour 6 notes données (en tableau ou via des champs texte) 
option : introduisez une pondération par coefficients (donnés ou via des champs texte)

EXERCICE 5 :
Calculer toutes les combinaisons possibles à partir des éléments d'un tableau de caractères :
montab = ["a","b","c"] => abc acb bac bca cab cba ab ac ....
(on fera en sorte que cette procédure soit dynamique, cad s'adapte à la taille du tableau)

EXERCICE 6 : VALIDE
Remplacer chaque caractère dans une chaine par son suivant dans l'alphabet

EXERCICE 7 : VALIDE
Calculer à partir d'un entier donné son equivalent : 
    - a/ en minutes
    - b/ en secondes

EXERCICE 8 : VALIDE
Concatener deux chaines sauf leur premiere lettre 

EXERCICE 9 : VALIDE
Trouver dans un tableau de chaines laquelle est la plus longue

EXERCICE 10 : VALIDE
Construire 3 tableaux d'entiers issus de la deconcatenation d'un tableau d'entiers à 3 chiffres 
(le premier chiffre va dans le premier tableau, le deuxieme chiffre dans le deuxieme tableau, ... ex : [123,456,789] => [147],[258],[369])
le tableau original doit etre au moins de taille 10 

EXERCICE 11 :
Construire un tableau aléatoire de taille 50 d'entiers a deux chiffres (0-99) et calculer le pourcentage d'apparition de chaque chiffre

EXERCICE 12 :
Trouver si dans deux chaines données, il est possible de ré-arranger l'une d'elles pour qu'elles soient egales
(c'est à dire vérifier qu'elles ont le même contenu, même dans le désordre)

EXERCICE 13 :
Determiner si un point (a,b) est contenu dans un cercle de centre x,y et de rayon r
