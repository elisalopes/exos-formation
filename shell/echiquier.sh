#! /bin/bash

for (( i=0; i<8; i++ ))
do
    for (( j=0; j<8; j++ ))
    do
        let "modulo=($j + $i) % 2" #alternate odd/even
        if [ $modulo -eq 0 ] #if even
        then
            echo -n "  " #will return black color
        else
            echo -ne "\e[1;47m  \e[0m" #White color
        fi
    done
    echo #New line
done
