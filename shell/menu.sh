#! /bin/bash
display_symbols () {  #use to add "-" between titles
    for (( i=0; i<$1; i++))
    do
        
        echo -n $2
    done
}

display_spaces () { #use to add spaces
    for (( i=0; i<$1; i++))
    do
        
        echo -n " "
    done
}

display_symbols 72 "#"
echo

while read line #read file
do
    if [ "$line" = "vide" ] #display an empty line of ---
    then
        echo -n "#" #add first hash
        display_symbols 70 "-"
        echo -n "#" # add second hash
        
    elif [[ "$line" == *"+"* ]] #look if there is a "+"" in the line
    then
        numero_plat=$(echo $line | cut -d "+" -f 1)
        plat=$(echo $line | cut -d "+" -f 2)
        prix=$(echo $line | cut -d "+" -f 3)
        echo -n "#" $numero_plat $plat 
        longueur="${#line}" #calcul line length
        display_spaces $((68-$longueur))
        echo -n $prix"€" "#"
    else #(entrees, plats, dessert, boissons) -> display titles
        echo -n "#"
        echo -n "$line"
        longueur="${#line}" #calcul title length
        display_spaces $((70-$longueur))
        echo -n "#"
    fi
    echo #new line
done < "carte.txt"
display_symbols 72 "#"
echo